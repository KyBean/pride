#[cfg(not(unix))]
compile_error!(
	"Program supports unix targets only. Requires accessing /dev/tty"
);

mod pride;
mod string;
mod terminal;

use clap::{command, Args, Parser};
use pride::*;

use crate::terminal::Terminal;

type Result<T> = core::result::Result<T, Box<dyn std::error::Error>>;

#[derive(Parser, Debug, Clone, Copy)]
#[command(author, version, about, long_about = None)]
struct Cli {
	flag: Flag,
	#[arg(long)]
	clear: bool,
}

#[derive(Args, Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
struct Banner {
	flag: Flag,
}

fn main() -> Result<()> {
	let mut terminal = Terminal::new(termion::get_tty()?);

	let cli = Cli::parse();

	if cli.clear {
		terminal.clear()?;
	}
	cli.flag.banner(&mut terminal)?;

	// let (chars_x, chars_y) = terminal.chars_dim()?;
	// let (pixels_x, pixels_y) = terminal.pixels_dim()?;
	// let (cell_x, cell_y) = terminal.cell_dim()?;
	// let cell_ratio = terminal.cell_ratio()?;
	//
	// println!("Terminal width (characters): ({chars_x}, {chars_y})");
	// println!("Terminal width (pixels): ({pixels_x}, {pixels_y})");
	// println!("Pixels per character cell: ({cell_x}, {cell_y})");
	// println!("Character cell aspect ratio: {cell_ratio}");

	Ok(())
}
