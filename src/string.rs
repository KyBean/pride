pub fn whitespace(len: usize) -> String {
	" ".repeat(len)
}
pub fn reset() -> termion::color::Bg<termion::color::Reset> {
	termion::color::Bg(termion::color::Reset)
}
