use std::os::fd::AsRawFd;

use clap::ValueEnum;
use termion::color;

use crate::terminal::Terminal;
use crate::{string, Result};

fn banner_color<Tty: AsRawFd>(
	terminal: &mut Terminal<Tty>,
	color: impl color::Color,
) -> Result<()> {
	use std::io::Write;
	let x_dim = terminal.chars_dim()?.0.into();
	writeln!(
		terminal,
		"{}{}{}",
		color::Bg(color),
		string::whitespace(x_dim),
		string::reset()
	)?;
	Ok(())
}

macro_rules! flags {
	($vis:vis flags { $($name:ident $(($($alias:ident),* $(,)?))? {$($colors:tt)*}),* $(,)? }) => {
		#[allow(non_camel_case_types)]
    	#[derive(
	        Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, ValueEnum,
        )]
	    $vis enum Flag {$(
	    	$name
	    	$(, $($alias),*)?
	    ),*}
        impl Flag {
            $vis fn banner<Tty: AsRawFd>(&self, terminal: &mut Terminal<Tty>) -> Result<()> {
                match self {
                    $(
                    	Self::$name $($(| Self::$alias)*)? => {
							colors!(terminal, $($colors)*);
						}
                    ),*
                }
                Ok(())
            }
	    }
    }
}

macro_rules! colors {
	($tty:ident, $color:ident, $($tail:tt)*) => {
	    	colors!($tty, $color);
		    colors!($tty, $($tail)*)
	};
	($tty:ident, ($r:literal, $g:literal, $b:literal), $($tail:tt)*) => {
	    colors!($tty, ($r, $g, $b));
		colors!($tty, $($tail)*)
	};
	($tty:ident, $color:ident $(,)?) => {
	   	banner_color($tty, color::$color)?
	};
	($tty:ident, ($r:literal, $g:literal, $b:literal) $(,)?) => {
	    banner_color($tty, color::Rgb($r, $g, $b))?
	};
}

flags! {
	pub flags {
		rainbow {
			(0xe5, 0, 0),
			(0xff, 0x8d, 0),
			(0xff, 0xee, 0),
			(0x02, 0x81, 0x21),
			(0, 0x4c, 0xff),
			(0x77, 0, 0x88)
		},
		transgender (trans) {
			(0x5b, 0xcf, 0xfb),
			(0xf5, 0xab, 0xb9),
			White,
			(0xf5, 0xab, 0xb9),
			(0x5b, 0xcf, 0xfb)
		},
		bisexual (bi) {
			(0xd6, 0x02, 0x70),
			(0xd6, 0x02, 0x70),
			(0x9b, 0x4f, 0x96),
			(0, 0x38, 0xa8),
			(0, 0x38, 0xa8)
		},
		pansexual (pan) {
			(0xff, 0x1c, 0x8d),
			(0xff, 0xd7, 0),
			(0x1a, 0xb3, 0xff)
		},
		nonbinary (enby) {
			(0xfc, 0xf4, 0x31),
			(0xfc, 0xfc, 0xfc),
			(0x9d, 0x59, 0xd2),
			(0x28, 0x28, 0x28)
		},
		lesbian (wlw) {
			(0xd6, 0x28, 0),
			(0xff, 0x9b, 0x56),
			White,
			(0xd4, 0x62, 0xa6),
			(0xa4, 0, 0x62)
		},
		agender {
			Black,
			(0xbc, 0xc4, 0xc7),
			White,
			(0xa5, 0xfa, 0x5e),
			White,
			(0xbc, 0xc4, 0xc7),
			Black
		},
		asexual (ace) {
			Black,
			(0xa4, 0xa4, 0xa4),
			White,
			(0x81, 0, 0x81)
		},
		genderqueer (gq) {
			(0xb5, 0x7f, 0xdd),
			White,
			(0x49, 0x82, 0x1e)
		},
		genderfluid (gf) {
			(0xfe, 0x76, 0xa2),
			White,
			(0xbf, 0x12, 0xd7),
			Black,
			(0x30, 0x3c, 0xbe)
		},
		aromantic (aro) {
			(0x3b, 0xa7, 0x40),
			(0xa8, 0xd4, 0x7a),
			White,
			(0xab, 0xab, 0xab),
			Black
		},
		gay (mlm) {
			(0x3d, 0x1a, 0x78),
			(0x50, 0x49, 0xcc),
			(0x7b, 0xad, 0xe2),
			White,
			(0x98, 0xe8, 0xc1),
			(0x26, 0xce, 0xaa),
			(0x07, 0x8d, 0x70)
		}
	}
}
