use std::io::{stdout, Stdout, Write};
use std::os::fd::AsRawFd;

use crate::Result;

pub struct Terminal<Tty> {
	tty: Tty,
	stdout: Stdout,
}

impl<Tty: AsRawFd> Terminal<Tty> {
	/// Constructs a new [`Terminal`] abstraction from the provided tty.
	///
	/// Note: It is assumed that the provided tty receives the bytes that are
	/// written to STDOUT.
	pub fn new(tty: Tty) -> Self {
		Self {
			tty,
			stdout: stdout(),
		}
	}

	/// The (x, y) dimensions of the terminal window in character units.
	///
	/// # Errors
	///
	/// Function Errors if:
	/// - This isn't run from a terminal.
	/// - Either reported dimension is 0.
	pub fn chars_dim(&self) -> Result<(u16, u16)> {
		let (x, y) = private::terminal_size(&self.tty)?;
		assert!(x != 0 && y != 0, "Reported size is 0");
		Ok((x, y))
	}

	/// The (x, y) dimensions of the terminal window in pixel units.
	///
	/// # Errors
	///
	/// Function errors if:
	/// - If this isn't run from a terminal.
	/// - Either reported dimension is 0.
	pub fn pixels_dim(&self) -> Result<(u16, u16)> {
		let (x, y) = private::terminal_size_pixels(&self.tty)?;
		assert!(x != 0 && y != 0, "Reported size is 0");
		Ok((x, y))
	}

	/// The (x, y) dimensions of individual character cells of the terminal
	/// window in pixel units.
	///
	/// # Errors
	///
	/// Function errors if either [`Self::chars_dim`] or
	/// [`Self::pixels_dim`] panics.
	pub fn cell_dim(&self) -> Result<(u16, u16)> {
		let (char_x, char_y) = self.chars_dim()?;
		let (pixel_x, pixel_y) = self.pixels_dim()?;
		Ok((pixel_x / char_x, pixel_y / char_y))
	}

	/// Width:Height ratio of the character cells of the terminal window.
	///
	/// # Errors
	///
	/// Function errors if [`Self::cell_dim`] errors.
	pub fn cell_ratio(&self) -> Result<f32> {
		let (x, y) = self.cell_dim()?;
		Ok(f32::from(x) / f32::from(y))
	}

	/// Clears the terminal window and moves the cursor to the upper left corner
	///
	/// # Errors
	///
	/// Function errors if an error occurs while writing bytes to STDOUT.
	pub fn clear(&mut self) -> Result<()> {
		write!(
			self,
			"{}{}",
			termion::clear::All,
			termion::cursor::Goto(1, 1)
		)?;
		Ok(())
	}
}

impl<Tty> Write for Terminal<Tty> {
	fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
		self.stdout.write(buf)
	}
	fn flush(&mut self) -> std::io::Result<()> {
		self.stdout.flush()
	}
}

mod private {
	use std::mem;
	use std::os::fd::AsRawFd;

	use libc::{c_ushort, ioctl, TIOCGWINSZ};

	use crate::Result;
	#[repr(C)]
	struct TermSize {
		row: c_ushort,
		col: c_ushort,
		x: c_ushort,
		y: c_ushort,
	}
	pub(super) fn terminal_size(tty: &impl AsRawFd) -> Result<(u16, u16)> {
		// Safety: Stolen from https://docs.rs/termion/2.0.1/src/termion/sys/unix/size.rs.html#14-20
		unsafe {
			let mut size: TermSize = mem::zeroed();
			if -1 == ioctl(tty.as_raw_fd(), TIOCGWINSZ, &mut size as *mut _) {
				Err(std::io::Error::last_os_error())?
			}
			Ok((size.col as u16, size.row as u16))
		}
	}
	pub(super) fn terminal_size_pixels(
		tty: &impl AsRawFd,
	) -> Result<(u16, u16)> {
		// Safety: Stolen from https://docs.rs/termion/2.0.1/src/termion/sys/unix/size.rs.html#23-29
		unsafe {
			let mut size: TermSize = mem::zeroed();
			if -1 == ioctl(tty.as_raw_fd(), TIOCGWINSZ, &mut size as *mut _) {
				Err(std::io::Error::last_os_error())?
			}
			Ok((size.x as u16, size.y as u16))
		}
	}
}
